# -*- coding: utf-8 -*-
"""
Created on Mon Oct 25 09:26:06 2021

@author: Stoyan Boyukliyski
"""

import sys

class TicTacToe():
    def __init__(self, board, player, bot):
        self.board = board
        self.player = player
        self.bot = bot
         
    def printBoard(self):
        print(self.board[1] + '|' + self.board[2] + '|' + self.board[3])
        print('-+-+-')
        print(self.board[4] + '|' + self.board[5] + '|' + self.board[6])
        print('-+-+-')
        print(self.board[7] + '|' + self.board[8] + '|' + self.board[9])
        print("\n")
    
    def checkForWin(self, mark):
        if self.board[1] == self.board[2] and self.board[1] == self.board[3] and self.board[1] == mark:
            return True
        elif (self.board[4] == self.board[5] and self.board[4] == self.board[6] and self.board[4] == mark):
            return True
        elif (self.board[7] == self.board[8] and self.board[7] == self.board[9] and self.board[7] == mark):
            return True
        elif (self.board[1] == self.board[4] and self.board[1] == self.board[7] and self.board[1] == mark):
            return True
        elif (self.board[2] == self.board[5] and self.board[2] == self.board[8] and self.board[2] == mark):
            return True
        elif (self.board[3] == self.board[6] and self.board[3] == self.board[9] and self.board[3] == mark):
            return True
        elif (self.board[1] == self.board[5] and self.board[1] == self.board[9] and self.board[1] == mark):
            return True
        elif (self.board[7] == self.board[5] and self.board[7] == self.board[3] and self.board[7] == mark):
            return True
        else:
            return False
        
    def checkForDraw(self):
        for key, value in self.board.items():
            if value == ' ':
                return False
        return True
    
    def spaceIsFree(self, position):
        if self.board[position] == ' ':
            return True
        else:
            return False    
    
    def MakeAMove(self, letter, position):
        if self.spaceIsFree(position):
            self.board[position] = letter
            self.printBoard()
            if self.checkForDraw():
                print("Draw!")
                sys.exit()
            elif self.checkForWin(letter):
                if letter == 'X':
                    print("Xs have won")
                    sys.exit()
                else:
                    print('Os have won')
                    sys.exit()
                    
            return
        else:
            print("Can't insert there!")
            position = int(input("Please enter new position:  "))
            self.MakeAMove(letter, position)
            return
        
    def minimax(self, depth, isMaximizing):
        if self.checkForWin(self.bot):
            return 100 - depth
        elif self.checkForWin(self.player):
            return -100 + depth
        elif self.checkForDraw():
            return 0
        else:
            if (isMaximizing):
                bestScore = -10**3
                for key in self.board.keys():
                    if (self.board[key] == ' '):
                        self.board[key] = self.bot
                        score = self.minimax(depth + 1, False)
                        self.board[key] = ' '
                        if (score > bestScore):
                            bestScore = score
                return bestScore
        
            else:
                bestScore = 10**3
                for key in self.board.keys():
                    if (self.board[key] == ' '):
                        self.board[key] = self.player
                        score = self.minimax(depth + 1, True)
                        self.board[key] = ' '
                        if (score < bestScore):
                            bestScore = score
                return bestScore
                        
    def Computer_Move(self):
        bestScore = -10**3
        bestMove = 0
        for key in self.board.keys():
            if (self.board[key] == ' '):
                self.board[key] = self.bot
                score = self.minimax(0, False)
                self.board[key] = ' '
                if (score > bestScore):
                    bestScore = score
                    bestMove = key
    
        self.MakeAMove(self.bot, bestMove)
        return
    
    def Player_Move(self):
        position = int(input("Enter the position for your marker:  "))
        self.MakeAMove(self.player, position)
        return
    
def Player_Marker():
    player = input('Select which marker you would like to be (X or O only): ')
    if player not in ['O', 'X']:
        print('Please choose again: ')
        player, bot = Player_Marker()
        return player, bot
    else:
        if player == 'X':
            bot = 'O'
            return player, bot
        
        elif player == 'O':
            bot = 'X'
            return player, bot
        
        else:
            raise('Error: Something Unexpected Happened!')

def Priority_Function():
    priority = input('Do you want to go first (Yes or No): ')
    if priority in ['Yes', 'No']:
        return priority
    else:
        print('Please enter again: ')
        priority = Priority_Function()
        return priority
    
def showMoves():
    print('These are the indexes for each cell of the board: ')
    print('1' + '|' + '2' + '|' + '3')
    print('-+-+-')
    print('4' + '|' + '5' + '|' + '6')
    print('-+-+-')
    print('7' + '|' + '8' + '|' + '9')
    print("\n")
    print('Have fun playing :)')