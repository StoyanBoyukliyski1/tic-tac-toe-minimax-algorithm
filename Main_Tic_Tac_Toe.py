# -*- coding: utf-8 -*-
"""
Created on Mon Oct 25 11:35:33 2021

@author: Stoyan Boyukliyski
"""

from my_minimax import TicTacToe as TTT
import my_minimax as mnm

mnm.showMoves()
    
board = {1: ' ', 2: ' ', 3: ' ',
         4: ' ', 5: ' ', 6: ' ',
         7: ' ', 8: ' ', 9: ' '}

player, bot = mnm.Player_Marker()

priority = mnm.Priority_Function()

game = mnm.TicTacToe(board, player, bot)

game.printBoard()

while not ((game.checkForWin(player) or game.checkForWin(bot)) or game.checkForDraw()):
    if priority == 'Yes':
        game.Player_Move()
        game.Computer_Move()
    else:
        game.Computer_Move()
        game.Player_Move()
    